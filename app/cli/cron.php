<?php

require 'cli.php';

$modelPrizes = Prizes::find();

foreach ($modelPrizes as $prize) {

    // Запуск транзакции
    $this->db->begin();

    $countPrize          = 0;
    $countGivenPrize     = 0;
    $countCancelledPrize = 0;
    $date                = new DateTime();
    $dateNow             = $date->format('Y-m-d');
    $dayBefore           = $date->modify('-1 day')->format('Y-m-d');
    $reserveDate         = $date->modify('-1 day')->format('Y-m-d');
    $modelPrizeReserve   = PrizeReserve::find([
        'conditions' => 'prize_id = :prize_id: AND date BETWEEN :reserveDate: AND :dayBefore:',
        'bind' => [
            'prize_id'    => $prize->id,
            'dayBefore'   => $dayBefore,
            'reserveDate' => $reserveDate,
        ],
    ]);

    if ($modelPrizeReserve) {
        foreach ($modelPrizeReserve as $prizeReserve) {

            // Количество призов с истёкшим сроком бронирования
            if (($prizeReserve->state == null)&&($prizeReserve->date == $reserveDate)) {
                $countCancelledPrize++;
            }

            // Количество призов которое забрали вчера
            if (($prizeReserve->state == 1)&&($prizeReserve->date == $dayBefore)) {
                $countGivenPrize++;
            }

            //$prizeReserve->delete();
        }
    }

    $modelPriseDistribution = PrizeDistribution::find([
        'conditions' => 'prize_id = :prize_id: AND date BETWEEN :dayBefore: AND :date:',
        'bind' => [
            'prize_id'  => $prize->id,
            'dayBefore' => $dayBefore,
            'date'      => $dateNow,
        ],
        'order' => 'date',
    ]);

    if ($modelPriseDistribution) {
        foreach ($modelPriseDistribution as $priseDistribution) {
            if ($priseDistribution->date == $dayBefore) {
                // Cчитаем остаток призов за предыдущий день, плюс не выданные за два дня назад
                $countPrize = $priseDistribution->balance+$countCancelledPrize+($priseDistribution->count-$countGivenPrize);
            }
            if ($priseDistribution->date == $dateNow) {
                $priseDistribution->balance = $countPrize;

                if ($priseDistribution->save() === false) {
                    $this->db->rollback();
                    exit(PHP_EOL . 'Что-то пошло не так' . PHP_EOL);
                }
            }
        }
    }

    // Фиксация транзакции
    $this->db->commit();
}

echo PHP_EOL . 'Успешно' . PHP_EOL;
