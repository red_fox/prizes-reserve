<?php

use Phalcon\Di\FactoryDefault;

if ('cli' !== PHP_SAPI) {
    ob_end_clean();
    header('HTTP/1.0 404 Not Found');
    exit;
}

mb_internal_encoding('utf8');
date_default_timezone_set('Asia/Yekaterinburg');
error_reporting(E_ALL);

$di = new FactoryDefault();

$config = require __DIR__ . '/../config/config.php';
require __DIR__ . '/../config/loader.php';
require __DIR__ . '/../config/services.php';
