<?php

$router = $di->getRouter(false);

$router->add("/campaign/{campaignId:[0-9]+}/", array(
    'controller'    => 'index',
    'action'        => 'campaign',
))->setName("campaign");

$router->add("/prize/{prizeId:[0-9]+}/", array(
    'controller'    => 'index',
    'action'        => 'prize',
))->setName("prize");

$router->add("/authorize/", array(
    'controller'    => 'index',
    'action'        => 'authorize',
))->setName("authorize");

$router->add("/reserve/", array(
    'controller'    => 'index',
    'action'        => 'reserve',
))->setName("reserve");

$router->add("/givingOut/{prizeId:[0-9]+}/", array(
    'controller'    => 'index',
    'action'        => 'givingOut',
))->setName("givingOut");

/*
 * API
 */
$router->addPost("/api/reserve/", array(
    'controller'    => 'api',
    'action'        => 'reserve',
))->setName("reserveApi");

$router->addPost("/api/cancel/", array(
    'controller'    => 'api',
    'action'        => 'cancel',
))->setName("cancelApi");


$router->addGet("/", array(
    'controller'    => 'index',
    'action'        => 'index',
))->setName("index");

$router->handle();
