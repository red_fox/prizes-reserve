<?php

class ApiController extends ControllerBase
{
    /*
     * Резерв призов пользователями
     */
    public function reserveAction()
    {
        $result = [
            'success' => false,
            'message' => 'Ошибка резервирования приза'
        ];

        if ($this->session->has('userId')) {

            $userId  = $this->session->get('userId');
            $prizeId = $this->request->getPost('prizeId');

            // Выбираем записи не старше вчерашнего дня
            $date        = new DateTime();
            $dateNow     = $date->format('Y-m-d');
            $reserveDate = $date->modify('-1 day')->format('Y-m-d');

            // Проверяем, бронировали мы этот приз ранее или нет
            $modelPrizeReserve = PrizeReserve::find([
                'conditions' => 'user_id = :user_id: AND prize_id = :prize_id: AND date BETWEEN :reserveDate: AND :dateNow:',
                'bind' => [
                    'user_id'     => $userId,
                    'prize_id'    => $prizeId,
                    'dateNow'     => $dateNow,
                    'reserveDate' => $reserveDate,
                ],
            ]);

            $checkAdd = true;
            if ($modelPrizeReserve) {

                foreach ($modelPrizeReserve as $prizeReserve) {
                    // Если есть забронированные призы или любая запись на сегодня
                    if (($prizeReserve->state == null)||($prizeReserve->date == $dateNow)) {
                        $result['message'] = 'Приз ранее уже бронировался';
                        $checkAdd = false;
                    }
                }
            }

            If ($checkAdd) {

                $this->db->execute('LOCK TABLES prize_reserve WRITE');

                // Запуск транзакции
                $this->db->begin();

                // Сколько призов уже забронировано
                $modelPrizeReserveCheck = PrizeReserve::find([
                    'conditions' => 'prize_id = :prize_id: AND date = :date:',
                    'bind' => [
                        'prize_id' => $prizeId,
                        'date'     => date('Y-m-d'),
                    ],
                ]);

                // Количество доступных для брони призов в текущий день
                $modelPrizeDistribution = PrizeDistribution::findFirst([
                    'conditions' => 'prize_id = :prize_id: AND date = :date:',
                    'bind' => [
                        'prize_id' => $prizeId,
                        'date'     => date('Y-m-d'),
                    ],
                ]);

                // Всего доступных призов на сегодня
                $totalAvailable = $modelPrizeDistribution->count + $modelPrizeDistribution->balance;

                // Есть ли доступные для брони призы
                if (($modelPrizeReserveCheck->count() <= $totalAvailable)&&($totalAvailable > 0)) {
                    $modelPrizeReserve = new PrizeReserve();

                    $modelPrizeReserve->user_id  = $userId;
                    $modelPrizeReserve->prize_id = $prizeId;
                    $modelPrizeReserve->date     = date('Y-m-d');

                    if ($modelPrizeReserve->save()) {
                        $result['success'] = true;
                        $result['message'] = 'Успешно зарезервировано';
                    } else {
                        $this->db->rollback();
                        $result['message'] = 'Ошибка сохранения';
                    }
                } else {
                    $result['message'] = 'К сожалению количество призов для брони закончилось';
                }

                // Фиксация транзакции
                $this->db->commit();

                $this->db->execute('UNLOCK TABLES');
            }
        }

        return json_encode($result);
    }

    /*
     * Отмена резерва
     */
    public function cancelAction()
    {
        $result = [
            'success' => false,
            'message' => 'Ошибка отмены брони'
        ];

        if ($this->session->has('userId')) {

            $userId  = $this->session->get('userId');
            $prizeId = $this->request->getPost('prizeId');

            // Выбираем записи не старше вчерашнего дня
            $date = new DateTime();

            // Пытаемся забронировать или отменить бронь?
            $modelPrizeReserve = PrizeReserve::findFirst([
                'conditions' => 'user_id = :user_id: AND prize_id = :prize_id: AND state IS NULL AND date BETWEEN :reserveDate: AND :dateNow:',
                'bind' => [
                    'user_id'     => $userId,
                    'prize_id'    => $prizeId,
                    'dateNow'     => $date->format('Y-m-d'),
                    'reserveDate' => $date->modify('-1 day')->format('Y-m-d'),
                ],
            ]);

            if ($modelPrizeReserve) {
                if ($modelPrizeReserve->delete()) {
                    $result['success'] = true;
                    $result['message'] = 'Бронь отменена';
                }
            }
        }

        return json_encode($result);
    }
}
