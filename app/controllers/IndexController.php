<?php

class IndexController extends ControllerBase
{

    /*
     * Главная
     */
    public function indexAction()
    {
        $modelCampaign = Campaign::find();

        $this->view->setVar('modelCampaign', $modelCampaign);
    }

    /*
     * Метод отображения страницы акции
     */
    public function campaignAction($campaignId)
    {
        $modelCampaign = Campaign::findFirst($campaignId);

        // Если пришел POST запрос
        if ($this->request->isPost()) {

            $modelCampaign->title = $this->request->getPost('title');
            $modelCampaign->start = $this->request->getPost('start');
            $modelCampaign->stop  = $this->request->getPost('stop');

            if ($modelCampaign->save()) {
                $this->flash->notice('Успешно сохранено');
                return $this->response->redirect(array('for' => 'campaign', 'campaignId' => $modelCampaign->id));
            }
        }

        $this->view->setVar('modelCampaign', $modelCampaign);
    }

    /*
     * Метод отображения страницы приза
     */
    public function prizeAction($prizeId)
    {
        $modelPrizes = Prizes::findFirst($prizeId);

        // Если пришел POST запрос
        if ($this->request->isPost()) {

            $distribution = $this->request->getPost('distribution');
            $distribution = json_decode($distribution);
            $isError      = false;

            foreach ($distribution as $item) {

                $modelPrizeDistribution = PrizeDistribution::findFirst([
                    'conditions' => 'prize_id = :prize_id: AND date = :date:',
                    'bind' => [
                        'prize_id' => $prizeId,
                        'date'     => $item[0],
                    ],
                ]);

                if ($modelPrizeDistribution) {
                    $modelPrizeDistribution->count = $item[1];
                } else {
                    $modelPrizeDistribution = new PrizeDistribution();
                    $modelPrizeDistribution->prize_id = $prizeId;
                    $modelPrizeDistribution->date     = $item[0];
                    $modelPrizeDistribution->count    = $item[1];
                    $modelPrizeDistribution->balance  = 0;
                }

                if (!$modelPrizeDistribution->save()) {
                    foreach ($modelPrizeDistribution->getMessages() as $message) {
                        $this->flash->error($message);
                        $isError = true;
                    }
                }
            }

            if (!$isError) {
                $this->flash->notice('Успешно сохранено');
            }
        }

        // Время действия акции в днях
        $startDate = new DateTime($modelPrizes->Campaign->start);
        $totalDays = $startDate->diff(new DateTime($modelPrizes->Campaign->stop))->format('%d');

        $distributionArray = [];
        foreach ($modelPrizes->PrizeDistribution as $distribution) {
            $distributionArray[$distribution->date] = $distribution->count;
        }

        // Формируем массив с датами и количеством призов (если есть)
        $resultArray = [];
        for ($i=0; $i<=$totalDays; $i++) {
            $date = $startDate->format("Y-m-d");
            $resultArray[$date] = 0;
            if (array_key_exists($date, $distributionArray)) {
                $resultArray[$date] = $distributionArray[$date];
            }
            $startDate->modify("+1 day");
        }

        $this->view->setVar('modelPrizes', $modelPrizes);
        $this->view->setVar('resultArray', $resultArray);
    }

    /*
     * Подобие авторизации
     */
    public function authorizeAction()
    {
        // Если пришел POST запрос
        if ($this->request->isPost()) {

            // Установка значения сессии
            $this->session->set('userId', $this->request->getPost('userId'));
        }
    }

    /*
     * Резерв призов пользователями
     */
    public function reserveAction()
    {
        if (!$this->session->has('userId')) {
            $this->flash->warning('Необходимо авторизоваться');
            return $this->response->redirect(array('for' => 'authorize'));
        }

        // Выбираем записи не старше вчерашнего дня
        $date        = new DateTime();
        $dateNow     = $date->format('Y-m-d');
        $reserveDate = $date->modify('-1 day')->format('Y-m-d');

        $modelCampaign     = Campaign::find();
        $modelPrizeReserve = PrizeReserve::find([
            'conditions' => 'user_id = :user_id: AND date BETWEEN :reserveDate: AND :dateNow:',
            'bind' => [
                'user_id'     => $this->session->get('userId'),
                'dateNow'     => $dateNow,
                'reserveDate' => $reserveDate,
            ],
        ]);

        // Массив с id призов пользователя
        $reservedPrizesIds   = [];
        $reservedPrizesState = [];
        foreach ($modelPrizeReserve as $prizeReserve) {
            // Вчерашние выданные не интересны
            if (($prizeReserve->date == $reserveDate)&&($prizeReserve->state == 1)) {
                continue;
            }
            $reservedPrizesIds[] = $prizeReserve->prize_id;
            $reservedPrizesState[$prizeReserve->prize_id] = $prizeReserve->state;
        }

        $this->view->setVar('modelCampaign', $modelCampaign);
        $this->view->setVar('reservedPrizesIds', $reservedPrizesIds);
        $this->view->setVar('reservedPrizesState', $reservedPrizesState);
    }

    /*
     * Выдача призов
     */
    public function givingOutAction($prizeId)
    {
        $modelPrizes = Prizes::findFirst($prizeId);

        // Если пришел POST запрос
        if ($this->request->isPost()) {
            $reserveId = $this->request->getPost('reserveid');
            $action    = $this->request->getPost('action');

            $modelPrizeReserve = PrizeReserve::findFirst($reserveId);

            switch ($action) {
                case 'give':
                    $modelPrizeReserve->state = 1;
                    $modelPrizeReserve->save();
                    break;
                case 'cancel':
                    $modelPrizeReserve->delete();
                    break;
            }
        }

        $this->view->setVar('modelPrizes', $modelPrizes);
    }

}
