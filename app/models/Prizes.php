<?php

class Prizes extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $campaign_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $title;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $count;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo("campaign_id", "Campaign", "id");

        $this->hasMany(
            "id",
            "PrizeReserve",
            "prize_id",
            array(
                'foreignKey' => array(
                    'action' => Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );

        $this->hasMany(
            "id",
            "PrizeDistribution",
            "prize_id",
            array(
                'foreignKey' => array(
                    'action' => Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'prizes';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Prizes[]|Prizes
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Prizes
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
