<?php

class Campaign extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $start;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $stop;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany(
            "id",
            "Prizes",
            "campaign_id",
            array(
                'foreignKey' => array(
                    'action' => Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'campaign';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Campaign[]|Campaign
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Campaign
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
