<?php

class PrizeDistribution extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $prize_id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $date;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $count;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $balance;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo("prize_id", "Prizes", "id");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'prize_distribution';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PrizeDistribution[]|PrizeDistribution
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PrizeDistribution
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
