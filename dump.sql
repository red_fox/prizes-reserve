CREATE TABLE IF NOT EXISTS `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `start` date NOT NULL,
  `stop` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `campaign` (`id`, `title`, `start`, `stop`) VALUES
(1, 'Акция 1', '2018-03-26', '2018-04-08');

CREATE TABLE IF NOT EXISTS `prizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `prizes` (`id`, `campaign_id`, `title`, `count`) VALUES
(1, 1, 'Приз 1', 100),
(2, 1, 'Приз 2', 200);

CREATE TABLE IF NOT EXISTS `prize_distribution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prize_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `count` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prize_id` (`prize_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

INSERT INTO `prize_distribution` (`id`, `prize_id`, `date`, `count`, `balance`) VALUES
(1, 1, '2018-03-26', 13, 0),
(2, 1, '2018-03-27', 15, 0),
(3, 1, '2018-03-28', 18, 0),
(4, 1, '2018-03-29', 10, 0),
(5, 1, '2018-03-30', 4, 0),
(6, 1, '2018-03-31', 5, 0),
(7, 1, '2018-04-02', 3, 0),
(8, 2, '2018-03-26', 20, 0),
(9, 2, '2018-03-27', 15, 0),
(10, 2, '2018-03-28', 20, 0),
(11, 2, '2018-03-29', 10, 0),
(12, 2, '2018-03-30', 20, 0),
(13, 2, '2018-03-31', 5, 0),
(14, 2, '2018-04-02', 20, 2),
(15, 1, '2018-04-01', 9, 0),
(16, 1, '2018-04-03', 3, 2),
(18, 1, '2018-04-05', 5, 0),
(19, 1, '2018-04-06', 3, 0),
(20, 1, '2018-04-07', 5, 0),
(21, 1, '2018-04-08', 7, 0),
(22, 1, '2018-04-09', 9, 0),
(23, 1, '2018-04-04', 2, 10),
(24, 2, '2018-04-01', 20, 0),
(25, 2, '2018-04-03', 30, 0),
(26, 2, '2018-04-04', 10, 35),
(27, 2, '2018-04-05', 26, 0),
(28, 2, '2018-04-06', 1, 0),
(29, 2, '2018-04-07', 1, 0),
(30, 2, '2018-04-08', 1, 0),
(31, 2, '2018-04-09', 1, 0);

CREATE TABLE IF NOT EXISTS `prize_reserve` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prize_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` tinyint(1) DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prize_id` (`prize_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

INSERT INTO `prize_reserve` (`id`, `prize_id`, `user_id`, `state`, `date`) VALUES
(6, 2, 6, 1, '2018-03-29'),
(11, 1, 6, NULL, '2018-03-29'),
(13, 1, 7, NULL, '2018-03-03'),
(44, 2, 5, NULL, '2018-03-30'),
(48, 1, 4564564, 1, '2018-04-02'),
(49, 2, 4564564, NULL, '2018-04-03'),
(50, 1, 34535, 1, '2018-04-02'),
(53, 1, 2432434, 1, '2018-04-02'),
(56, 1, 789, NULL, '2018-04-02'),
(57, 1, 80890, NULL, '2018-04-02'),
(59, 1, 4453, NULL, '2018-04-02'),
(60, 1, 54354, NULL, '2018-04-02'),
(61, 1, 43234, NULL, '2018-04-02'),
(63, 2, 423234, NULL, '2018-04-02'),
(64, 2, 78989789, NULL, '2018-04-02'),
(66, 1, 345345, NULL, '2018-04-02'),
(67, 2, 3233, NULL, '2018-04-02'),
(74, 2, 12, NULL, '2018-04-02'),
(76, 2, 7777, NULL, '2018-04-02'),
(77, 1, 12, 1, '2018-04-04');
